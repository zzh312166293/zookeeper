package com.lagou.study.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.lagou.study.util.SpringContextUtil;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DruidConfig {

    @Bean(name = "dataSource")
    public DataSource configureDataSource() throws Exception {
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(1000)
                .connectionTimeoutMs(1000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 1000))
                .build();
        curatorFramework.start();
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(new String(curatorFramework.getData().forPath("/datasource/driver-class-name")));
        dataSource.setUrl(new String(curatorFramework.getData().forPath("/datasource/url")));
        dataSource.setUsername(new String(curatorFramework.getData().forPath("/datasource/username")));
        dataSource.setPassword(new String(curatorFramework.getData().forPath("/datasource/password")));
        curatorFramework.close();
        return dataSource;
    }


    @Bean(name = "transactionManager")
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new JpaTransactionManager();
    }

}
