package com.lagou.study.service;

import com.lagou.study.pojo.TbResume;

import java.util.List;

public interface TbResumeService {
    List<TbResume> list();

    void add(TbResume tbResume);

    void edit(TbResume tbResume);

    void delete(Long id);

    TbResume get(Long id);
}
