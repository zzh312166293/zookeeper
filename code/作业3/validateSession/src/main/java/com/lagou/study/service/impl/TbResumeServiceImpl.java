package com.lagou.study.service.impl;

import com.lagou.study.mapper.TbResumeMapper;
import com.lagou.study.pojo.TbResume;
import com.lagou.study.service.TbResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TbResumeServiceImpl implements TbResumeService {

    @Autowired
    private TbResumeMapper tbResumeMapper;

    @Override
    public List<TbResume> list() {
        return tbResumeMapper.findAll();
    }

    @Override
    public void add(TbResume tbResume) {
        tbResumeMapper.save(tbResume);
    }

    @Override
    public void edit(TbResume tbResume) {
        tbResumeMapper.save(tbResume);
    }

    @Override
    public void delete(Long id) {
        tbResumeMapper.deleteById(id);
    }

    @Override
    public TbResume get(Long id) {
        Optional<TbResume> tbResume = tbResumeMapper.findById(id);
        return tbResume.get();
    }
}
