package com.lagou.study.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        if(username==null||"".equals(username)){
            username = session.getAttribute("username")==null?"":session.getAttribute("username").toString().trim();
        }
        if(password==null||"".equals(password)){
            password = session.getAttribute("password")==null?"":session.getAttribute("password").toString().trim();
        }
        if("admin".equals(username) && "admin".equals(password)){
            session.setAttribute("username", "admin");
            session.setAttribute("password", "admin");
            return true;
        }else {
            response.sendRedirect("/login");
        }
        return false;
    }
}
