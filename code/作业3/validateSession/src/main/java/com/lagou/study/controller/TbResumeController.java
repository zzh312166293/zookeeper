package com.lagou.study.controller;

import com.lagou.study.pojo.TbResume;
import com.lagou.study.service.TbResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/loginCRUD")
public class TbResumeController {

    @Autowired
    private TbResumeService tbResumeService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request, HttpServletResponse response, Model model){
        List<TbResume> list = tbResumeService.list();
        model.addAttribute("list", list);
        return "list";
    }

    @RequestMapping("/add")
    public String add(HttpServletRequest request, HttpServletResponse response, Model model){
        return "add";
    }

    @RequestMapping("/edit")
    public String edit(Model model, Long id){
        TbResume tbResume = tbResumeService.get(id);
        model.addAttribute("tbResume",tbResume);
        return "edit";
    }

    @RequestMapping("/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Model model, TbResume tbResume){
        if(tbResume.getId()!=null&&!"".equals(tbResume.getId().toString().trim())){
            tbResumeService.edit(tbResume);
        }else{
            tbResumeService.add(tbResume);
        }
        return "redirect:list";
    }

    @RequestMapping("/delete")
    public String delete(HttpServletRequest request, HttpServletResponse response, Model model, Long id){
        tbResumeService.delete(id);
        return "redirect:list";
    }
}
