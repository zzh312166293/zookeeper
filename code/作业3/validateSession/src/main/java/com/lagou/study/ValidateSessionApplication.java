package com.lagou.study;

import com.lagou.study.config.ZkConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableCaching
public class ValidateSessionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValidateSessionApplication.class, args);
        ZkConfig zkConfig = new ZkConfig();
    }

}
