package com.lagou.study.mapper;

import com.lagou.study.pojo.TbResume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TbResumeMapper extends JpaRepository<TbResume, Long>, JpaSpecificationExecutor<TbResume> {

}
