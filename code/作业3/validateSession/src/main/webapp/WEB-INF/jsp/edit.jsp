<%--
  Created by IntelliJ IDEA.
  User: zhaoyuxuan
  Date: 2020/5/16
  Time: 18:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@page isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新增</title>
</head>
<body>
<form action="/loginCRUD/save" method="post">
    <table>
        <tr>
            <td>
                <input type="hidden" name="id" value="${tbResume.id}" />
            </td>
            <td>
                address：<input type="text" name="address" value="${tbResume.address}" >
            </td>
            <td>
                name：<input type="text" name="name" value="${tbResume.name}" />
            </td>
            <td>
                phone：<input type="text" name="phone" value="${tbResume.phone}" />
            </td>
            <td>
                <input type="submit" value="保存">
                <input type=”button” value="返回" onclick="javascript:history.back(-1);">
            </td>
        </tr>
    </table>
</form>
</body>
</html>
