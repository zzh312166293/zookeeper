<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<%--
  Created by IntelliJ IDEA.
  User: zhaoyuxuan
  Date: 2020/5/16
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Title</title>
</head>
<body>
<table>
    <center><a href="/loginCRUD/add">新增</a></center>
    <tr>
        <td>
            id
        </td>
        <td>
            address
        </td>
        <td>
            name
        </td>
        <td>
            phone
        </td>
        <td>
            操作
        </td>
    </tr>
    <c:forEach items="${list}" var="tbResume">
        <tr>
            <td>
                ${tbResume.id}
            </td>
            <td>
                ${tbResume.address}
            </td>
            <td>
                ${tbResume.name}
            </td>
            <td>
                ${tbResume.phone}
            </td>
            <td>
                <a href="/loginCRUD/edit?id=${tbResume.id}">编辑</a>
                <a href="/loginCRUD/delete?id=${tbResume.id}">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
